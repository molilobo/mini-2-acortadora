from django.db import models


# Create your models here.
class ContentDB(models.Model):
    clave = models.CharField(max_length=6)
    valor = models.TextField()

    def __str__(self):
        return self.clave.__str__() + " -> " + self.valor


class Recurso(models.Model):
    ident = models.IntegerField(default=0)
    num = models.IntegerField(default=1)

    def __str__(self):
        return self.num.__str__()
