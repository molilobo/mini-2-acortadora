from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect

from .models import ContentDB, Recurso
from django.template import loader


# Create your views here.
def comprobarUrl(url):# Comprueba la url y devuelve la url completa

  if not url.startswith("http"):
    url = "https://" + url

  return url


def cargarTemplate(request):# Carga la plantilla
    contenidos = ContentDB.objects.all()

    
    template = loader.get_template('urlTemplates.html')
    context = {
        'contenidos': contenidos,
    }
    return HttpResponse(template.render(context, request))


def create_short():#deberia funcionar
    try:
        identificador = Recurso.objects.get(ident=0)

    except Recurso.DoesNotExist:
        identificador = Recurso(ident=0, num=1)
        identificador.save()

    short = str(identificador.num)

    identificador.num += 1
    identificador.save()

    return short


@csrf_exempt
def get_content(request):#definicion de los metods de post y get para obtener contenido
    try:
        if request.method == "POST":

            if ("url" in request.POST) and request.POST["url"] != "":
                valor = comprobarUrl(request.POST["url"])
                content = ContentDB.objects.get(valor=valor)

                if "short" in request.POST:
                    if request.POST["short"] != "":
                        short = request.POST["short"]

                    else:
                        short = create_short()

                    content.clave = short  # Sobreescribir el valor de la url acortada
                    content.save()

        elif request.method == "GET":
            valor = request.GET.get("url")

            if (valor is not None) and (valor != ""):
                valor = comprobarUrl(valor)
                content = ContentDB.objects.get(valor=valor)
                short = request.GET.get("short")

                if short is not None:

                    if short == "":
                            short = create_short()

                    content.clave = short  # Sobreescribir el valor de la url acortada
                    content.save()


        response = cargarTemplate(request)

        return response

    except ContentDB.DoesNotExist:#definicion de metodods  en caso de que no exista el contenido
        if request.method == "POST":

            if "url" in request.POST and request.POST["url"] != "":
                valor = comprobarUrl(request.POST["url"])

                if "short" in request.POST:

                    if request.POST["short"] != "":
                        short = request.POST["short"]
                        new_content = ContentDB(clave=short, valor=valor)
                        new_content.save()

                    else:
                        short = create_short()
                        new_content = ContentDB(clave=short, valor=valor)
                        new_content.save()

            response = cargarTemplate(request)
            return response

        elif request.method == "GET":
            valor = request.GET.get("url")

            if (valor is not None) and (valor != ""):

                    valor = comprobarUrl(valor)
                    short = request.GET.get("short")

                    if short is not None:
                        if short == "":
                            short = create_short()

                        new_content = ContentDB(clave=short, valor=valor)
                        new_content.save()

            response = cargarTemplate(request)

            return response
        else:
            raise Http404("No contentg.")


@csrf_exempt
def redirectUser(request, redirection):
    try:
        content = ContentDB.objects.get(clave=redirection)
        redirect_url = redirect(content.valor)

        return redirect_url

    except ContentDB.DoesNotExist:
        raise Http404("No existe el contenido.")
