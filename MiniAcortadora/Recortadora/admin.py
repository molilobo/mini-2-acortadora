from django.contrib import admin
from .models import ContentDB, Recurso

# Register your models here.
admin.site.register(ContentDB)
admin.site.register(Recurso)
